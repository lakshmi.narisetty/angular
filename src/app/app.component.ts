﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import {UserService, AuthenticationService } from './_services';
import { User } from './_models';

import './_content/app.less';
import {NgxPaginationModule} from 'ngx-pagination';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    currentUser: User;
    isAuditor : String;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
        
    }

    logout(id : number) {
        console.log(id);
        this.userService.logout(id).subscribe(() => {
            console.log("logout scuccessfully.");
        });
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}